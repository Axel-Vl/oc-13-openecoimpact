import { mount, createLocalVue } from '@vue/test-utils'
import Buefy from 'buefy'
import VanForm from '@/components/Calculator/GroundVehicles/VanForm'

const localVue = createLocalVue()
localVue.use(Buefy)

describe('Computed properties', function () {
  describe('isPropulsionTypeFieldVisible', function () {
    it('should return true', function () {
      const wrapper = mount(VanForm, { localVue })
      wrapper.setData({ distanceTravelled: 100 })
      expect(wrapper.vm.isPropulsionTypeFieldVisible).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(VanForm, { localVue })
      expect(wrapper.vm.isPropulsionTypeFieldVisible).toBe(false)
    })
  })

  describe('isClassTypeFieldVisible', function () {
    it('should return true', function () {
      const wrapper = mount(VanForm, { localVue })
      wrapper.setData({ propulsionType: 'diesel' })
      expect(wrapper.vm.isClassTypeFieldVisible).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(VanForm, { localVue })
      expect(wrapper.vm.isClassTypeFieldVisible).toBe(false)
    })
  })

  describe('canSubmit', function () {
    it('should return true', function () {
      const wrapper = mount(VanForm, { localVue })
      wrapper.setData({ classType: 'average' })
      expect(wrapper.vm.canSubmit).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(VanForm, { localVue })
      expect(wrapper.vm.canSubmit).toBe(false)
    })
  })
})

describe('Methods', function () {
  describe('getVanEmissionResults', function () {
    it('should call to the plugins and emit an event with the response as payload', async function () {
      const wrapper = mount(VanForm, {
        localVue,
        mocks: {
          $axios: {
            $get: jest.fn(() => Promise.resolve({ test: 'test' }))
          }
        }
      })
      await wrapper.vm.getVanEmissionsResults()
      expect(wrapper.emitted('result')).toEqual([[{ test: 'test' }]])
    })

    it('Should call to window alert because promise failed', async function () {
      const wrapper = mount(VanForm, {
        localVue,
        mocks: {
          $axios: {
            $get: jest.fn(() => Promise.reject(Error('test')))
          }
        }
      })
      jest.spyOn(window, 'alert').mockImplementationOnce(() => 'test')

      await wrapper.vm.getVanEmissionsResults()
      expect(wrapper.emitted('result')).toEqual([['test']])
      expect(window.alert).toBeCalled()
    })
  })
})
