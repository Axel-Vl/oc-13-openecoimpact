import { mount, createLocalVue } from '@vue/test-utils'
import Buefy from 'buefy'
import CarForm from '@/components/Calculator/GroundVehicles/CarForm'

const localVue = createLocalVue()
localVue.use(Buefy)

describe('Computed properties', function () {
  describe('isPropulsionTypeFieldVisible', function () {
    it('should return true', function () {
      const wrapper = mount(CarForm, { localVue })
      wrapper.setData({ distanceTravelled: 100 })
      expect(wrapper.vm.isPropulsionTypeFieldVisible).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(CarForm, { localVue })
      expect(wrapper.vm.isPropulsionTypeFieldVisible).toBe(false)
    })
  })

  describe('isMarketSegmentKnownFieldVisible', function () {
    it('should return true', function () {
      const wrapper = mount(CarForm, { localVue })
      wrapper.setData({ propulsionType: 'diesel' })
      expect(wrapper.vm.isMarketSegmentKnownFieldVisible).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(CarForm, { localVue })
      expect(wrapper.vm.isMarketSegmentKnownFieldVisible).toBe(false)
    })
  })

  describe('isMarketSegmentFieldVisible', function () {
    it('should return true', function () {
      const wrapper = mount(CarForm, { localVue })
      wrapper.setData({ marketSegmentKnown: true })
      expect(wrapper.vm.isMarketSegmentFieldVisible).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(CarForm, { localVue })
      expect(wrapper.vm.isMarketSegmentFieldVisible).toBe(false)
    })
  })

  describe('isSizeKnownFieldVisible', function () {
    it('should return true', function () {
      const wrapper = mount(CarForm, { localVue })
      wrapper.setData({ marketSegmentKnown: false })
      expect(wrapper.vm.isSizeKnownFieldVisible).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(CarForm, { localVue })
      expect(wrapper.vm.isSizeKnownFieldVisible).toBe(false)
    })
  })

  describe('isSizeFieldVisible', function () {
    it('should return true', function () {
      const wrapper = mount(CarForm, { localVue })
      wrapper.setData({
        sizeKnown: true,
        marketSegmentKnown: false
      })
      expect(wrapper.vm.isSizeFieldVisible).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(CarForm, { localVue })
      expect(wrapper.vm.isSizeFieldVisible).toBe(false)
    })
  })

  describe('canSubmit', function () {
    it('should return true when marketSegment got a value and vuelidate is valid', function () {
      const wrapper = mount(CarForm, {
        localVue,
        mocks: {
          $v: {
            $invalid: false
          }
        }
      })
      wrapper.vm.$v.$invalid = false
      wrapper.setData({
        marketSegment: 'mini'
      })
      expect(wrapper.vm.canSubmit).toBe(true)
    })
    it('should return true when size got a value and vuelidate is valid', function () {
      const wrapper = mount(CarForm, {
        localVue,
        mocks: {
          $v: {
            $invalid: false
          }
        }
      })
      wrapper.vm.$v.$invalid = false
      wrapper.setData({
        size: 'small'
      })
      expect(wrapper.vm.canSubmit).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(CarForm, { localVue })
      expect(wrapper.vm.canSubmit).toBe(false)
    })
  })
})

describe('Methods', function () {
  describe('getCarEmissionResults', function () {
    it('should call to the plugins and emit an event with the response as payload', async function () {
      const wrapper = mount(CarForm, {
        localVue,
        mocks: {
          $axios: {
            $get: jest.fn(() => Promise.resolve({ test: 'test' }))
          }
        }
      })
      await wrapper.vm.getCarEmissionsResults()
      expect(wrapper.emitted('result')).toEqual([[{ test: 'test' }]])
    })

    it('Should call to window alert because promise failed', async function () {
      const wrapper = mount(CarForm, {
        localVue,
        mocks: {
          $axios: {
            $get: jest.fn(() => Promise.reject(Error('test')))
          }
        }
      })
      jest.spyOn(window, 'alert').mockImplementationOnce(() => 'test')

      await wrapper.vm.getCarEmissionsResults()
      expect(wrapper.emitted('result')).toEqual([['test']])
      expect(window.alert).toBeCalled()
    })
  })
})
