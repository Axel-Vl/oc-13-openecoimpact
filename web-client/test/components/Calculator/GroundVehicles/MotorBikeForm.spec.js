import { mount, createLocalVue } from '@vue/test-utils'
import Buefy from 'buefy'
import MotorbikeForm from '@/components/Calculator/GroundVehicles/MotorbikeForm'

const localVue = createLocalVue()
localVue.use(Buefy)

describe('Computed properties', function () {
  describe('isSizeFieldVisible', function () {
    it('should return true', function () {
      const wrapper = mount(MotorbikeForm, { localVue })
      wrapper.setData({ distanceTravelled: 100 })
      expect(wrapper.vm.isSizeFieldVisible).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(MotorbikeForm, { localVue })
      expect(wrapper.vm.isSizeFieldVisible).toBe(false)
    })
  })

  describe('canSubmit', function () {
    it('should return true', function () {
      const wrapper = mount(MotorbikeForm, { localVue })
      wrapper.setData({ distanceTravelled: 100 })
      expect(wrapper.vm.canSubmit).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(MotorbikeForm, { localVue })
      expect(wrapper.vm.canSubmit).toBe(false)
    })
  })
})

describe('Methods', function () {
  describe('getMotorbikeEmissionsResults', function () {
    it('should call to the plugins and emit an event with the response as payload', async function () {
      const wrapper = mount(MotorbikeForm, {
        localVue,
        mocks: {
          $axios: {
            $get: jest.fn(() => Promise.resolve({ test: 'test' }))
          }
        }
      })

      await wrapper.vm.getMotorbikeEmissionsResults()
      expect(wrapper.emitted('result')).toEqual([[{ test: 'test' }]])
    })

    it('Should call to window alert because promise failed', async function () {
      const wrapper = mount(MotorbikeForm, {
        localVue,
        mocks: {
          $axios: {
            $get: jest.fn(() => Promise.reject(Error('test')))
          }
        }
      })
      jest.spyOn(window, 'alert').mockImplementationOnce(() => 'test')

      await wrapper.vm.getMotorbikeEmissionsResults()
      expect(wrapper.emitted('result')).toEqual([['test']])
      expect(window.alert).toBeCalled()
    })
  })
})
