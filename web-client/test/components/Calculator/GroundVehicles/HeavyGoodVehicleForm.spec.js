import { mount, createLocalVue } from '@vue/test-utils'
import Buefy from 'buefy'
import HeavyGoodVehicleForm from '@/components/Calculator/GroundVehicles/HeavyGoodVehicleForm'

const localVue = createLocalVue()
localVue.use(Buefy)

describe('Computed properties', function () {
  describe('isTrailerTypeFieldVisible', function () {
    it('should return true', function () {
      const wrapper = mount(HeavyGoodVehicleForm, { localVue })
      wrapper.setData({ distanceTravelled: 100 })
      expect(wrapper.vm.isTrailerTypeFieldVisible).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(HeavyGoodVehicleForm, { localVue })
      expect(wrapper.vm.isTrailerTypeFieldVisible).toBe(false)
    })
  })

  describe('isTrailerCapacityFieldVisible', function () {
    it('should return true', function () {
      const wrapper = mount(HeavyGoodVehicleForm, { localVue })
      wrapper.setData({ trailerType: 'rigid' })
      expect(wrapper.vm.isTrailerCapacityFieldVisible).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(HeavyGoodVehicleForm, { localVue })
      expect(wrapper.vm.isTrailerCapacityFieldVisible).toBe(false)
    })
  })

  describe('isLadenFieldVisible', function () {
    it('should return true', function () {
      const wrapper = mount(HeavyGoodVehicleForm, { localVue })
      wrapper.setData({ rigidTrailerCapacity: 'average' })
      expect(wrapper.vm.isLadenFieldVisible).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(HeavyGoodVehicleForm, { localVue })
      expect(wrapper.vm.isTrailerTypeFieldVisible).toBe(false)
    })
  })

  describe('isRefrigeratedFieldVisible', function () {
    it('should return true', function () {
      const wrapper = mount(HeavyGoodVehicleForm, { localVue })
      wrapper.setData({ laden: 'average' })
      expect(wrapper.vm.isRefrigeratedFieldVisible).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(HeavyGoodVehicleForm, { localVue })
      expect(wrapper.vm.isRefrigeratedFieldVisible).toBe(false)
    })
  })

  describe('isSubmitButtonVisible', function () {
    it('should return true', function () {
      const wrapper = mount(HeavyGoodVehicleForm,
        {
          localVue,
          mocks: {
            $v: {
              $invalid: false
            }
          }
        })
      expect(wrapper.vm.isSubmitButtonVisible).toBe(true)
    })
    it('should return false', function () {
      const wrapper = mount(HeavyGoodVehicleForm, { localVue })
      expect(wrapper.vm.isSubmitButtonVisible).toBe(false)
    })
  })

  describe('normalTrailerCapacity', function () {
    it('should return capacity based on trailerType value (rigid)', function () {
      const wrapper = mount(HeavyGoodVehicleForm, { localVue })
      wrapper.setData({ trailerType: 'rigid', rigidTrailerCapacity: 'average' })

      const testValue = wrapper.vm.normalTrailerCapacity
      expect(testValue).toBe('average')
    })

    it('should return capacity based on trailerType value (articulated)', function () {
      const wrapper = mount(HeavyGoodVehicleForm, { localVue })
      wrapper.setData({ trailerType: 'articulated', articulatedTrailerCapacity: 'average' })

      const testValue = wrapper.vm.normalTrailerCapacity
      expect(testValue).toBe('average')
    })

    it('should return nothing', function () {
      const wrapper = mount(HeavyGoodVehicleForm, { localVue })
      wrapper.setData({ trailerType: 'test' })

      const testValue = wrapper.vm.normalTrailerCapacity
      expect(testValue).toBe(undefined)
    })
  })
})

describe('Watch properties', function () {
  describe('trailerType', function () {
    it('should modify articulated trailer capacity and laden to null', async function () {
      const wrapper = mount(HeavyGoodVehicleForm, {
        localVue,
        data: () => ({
          distanceTravelled: '1234',
          trailerType: 'articulated',
          articulatedTrailerCapacity: 'average',
          laden: 'average'
        })
      })

      wrapper.setData({ trailerType: 'rigid' })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.articulatedTrailerCapacity).toBe(null)
      expect(wrapper.vm.laden).toBe(null)
    })

    it('should modify rigid trailer capacity and laden to null', async function () {
      const wrapper = mount(HeavyGoodVehicleForm, {
        localVue,
        data: () => ({
          distanceTravelled: '1234',
          trailerType: 'rigid',
          rigidTrailerCapacity: 'average',
          laden: 'average'
        })
      })

      wrapper.setData({ trailerType: 'articulated' })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.rigidTrailerCapacity).toBe(null)
      expect(wrapper.vm.laden).toBe(null)
    })

    it('should modify laden to null only', async function () {
      const wrapper = mount(HeavyGoodVehicleForm, {
        localVue,
        data: () => ({
          distanceTravelled: '1234',
          trailerType: 'rigid',
          rigidTrailerCapacity: 'average',
          articulatedTrailerCapacity: 'average',
          laden: 'average'
        })
      })

      wrapper.setData({ trailerType: undefined })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.rigidTrailerCapacity).toBe(null)
      expect(wrapper.vm.articulatedTrailerCapacity).toBe(null)
      expect(wrapper.vm.laden).toBe(null)
    })
  })
})

describe('Methods', function () {
  describe('getHGVEmissionResults', function () {
    it('should modify params based on trailer type and call to the plugins and emit an event with the response as payload (rigid)', async function () {
      const wrapper = mount(HeavyGoodVehicleForm, {
        localVue,
        data: () => ({
          distanceTravelled: '1234',
          trailerType: 'rigid',
          rigidTrailerCapacity: 'average',
          laden: 'average'
        }),
        mocks: {
          $axios: {
            $get: jest.fn(() => Promise.resolve({ test: 'test' }))
          }
        }
      })
      await wrapper.vm.getHGVEmissionsResults()
      expect(wrapper.emitted('result')).toEqual([[{ test: 'test' }]])
    })

    it('should modify params based on trailer type and call to the plugins and emit an event with the response as payload (articulated)', async function () {
      const wrapper = mount(HeavyGoodVehicleForm, {
        localVue,
        data: () => ({
          distanceTravelled: '1234',
          trailerType: 'articulated',
          articulatedTrailerCapacity: 'average',
          laden: 'average'
        }),
        mocks: {
          $axios: {
            $get: jest.fn(() => Promise.resolve({ test: 'test' }))
          }
        }
      })
      await wrapper.vm.getHGVEmissionsResults()
      expect(wrapper.emitted('result')).toEqual([[{ test: 'test' }]])
    })

    it('should not modify params based on trailer type', async function () {
      const wrapper = mount(HeavyGoodVehicleForm, {
        localVue,
        data: () => ({
          distanceTravelled: '1234',
          trailerType: undefined
        }),
        mocks: {
          $axios: {
            $get: jest.fn(() => Promise.resolve({ test: 'test' }))
          }
        }
      })
      await wrapper.vm.getHGVEmissionsResults()
      expect(wrapper.emitted('result')).toEqual([[{ test: 'test' }]])
    })

    it('Should call to window alert because promise failed', async function () {
      const wrapper = mount(HeavyGoodVehicleForm, {
        localVue,
        data: () => ({
          distanceTravelled: '1234',
          trailerType: undefined
        }),
        mocks: {
          $axios: {
            $get: jest.fn(() => Promise.reject(Error('test')))
          }
        }
      })
      jest.spyOn(window, 'alert').mockImplementationOnce(() => 'test')

      await wrapper.vm.getHGVEmissionsResults()
      expect(wrapper.emitted('result')).toEqual([['test']])
      expect(window.alert).toBeCalled()
    })
  })
})
