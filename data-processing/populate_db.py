import os
import re
import csv
from mongoengine import *

MONGODB_HOST = os.environ.get("MONGODB_HOST")


class FuelsConversionFactors(Document):
    tags = DictField()
    unit_of_measure = StringField(required=True)
    green_house_gas = StringField(required=True)
    green_house_gas_uom = StringField(required=True)
    conversion_factor = FloatField(required=True)


class PersonalCarDistanceConversionFactors(Document):
    tags = DictField()
    unit_of_measure = StringField(required=True)
    green_house_gas = StringField(required=True)
    green_house_gas_uom = StringField(required=True)
    conversion_factor = FloatField(required=True)


class PersonalMotorbikeDistanceConversionFactors(Document):
    tags = DictField()
    unit_of_measure = StringField(required=True)
    green_house_gas = StringField(required=True)
    green_house_gas_uom = StringField(required=True)
    conversion_factor = FloatField(required=True)


class FreightVehiclesVansDistanceConversionFactors(Document):
    tags = DictField()
    unit_of_measure = StringField(required=True)
    green_house_gas = StringField(required=True)
    green_house_gas_uom = StringField(required=True)
    conversion_factor = FloatField(required=True)


class FreightVehiclesHeavyGoodVehicleDistanceConversionFactors(Document):
    tags = DictField()
    unit_of_measure = StringField(required=True)
    green_house_gas = StringField(required=True)
    green_house_gas_uom = StringField(required=True)
    conversion_factor = FloatField(required=True)


def populate_db():
    # Connect to the local host database
    connect(host=f"mongodb://{MONGODB_HOST}")

    # Drop all existing tables
    FuelsConversionFactors.drop_collection()
    PersonalCarDistanceConversionFactors.drop_collection()
    PersonalMotorbikeDistanceConversionFactors.drop_collection()
    FreightVehiclesVansDistanceConversionFactors.drop_collection()
    FreightVehiclesHeavyGoodVehicleDistanceConversionFactors.drop_collection()

    with open("sources/uk_government_ghg_conversion_factors.csv") as csvfile:
        reader = csv.reader(csvfile, delimiter=";")

        for row in reader:
            if row[1] == "Fuels":
                extract_fuel_conversion_factors(row)

            elif row[1] == "Passenger vehicles":
                if row[2] != "Motorbike":
                    extract_personal_car_distance_conversion_factors(row)
                elif row[2] == "Motorbike":
                    extract_personal_motorbike_distance_conversion_factors(row)

            elif row[1] == "Delivery vehicles":
                if row[2] == "Vans":
                    extract_delivery_vehicles_vans_conversion_factors(row)
                elif row[2] != "Vans":
                    extract_delivery_vehicles_heavy_good_vehicle_conversion_factors(row)


def print_row_description(row):
    print("\n")
    print("-" * 200)
    print(row)


def extract_fuel_conversion_factors(row):
    print_row_description(row)

    try:
        if row[2] == "Gaseous fuels":
            element_state = "gaseous"
        elif row[2] == "Liquid fuels":
            element_state = "liquid"
        elif row[2] == "Solid fuels":
            element_state = "solid"

        # TODO add a filter between gross CV and net cv
        name = (
            row[3]
                .lower()
                .replace("-", "_")
                .replace("(", "")
                .replace(")", "")
                .replace(" ", "_")
                .replace("___", "_")
                .replace("__", "_")
        )

        tags = {
            "element_state": element_state,
            "name": name,
        }
    except IndexError:
        print("tags fail")
        pass

    try:
        unit_of_measure = row[6]
    except IndexError:
        print("unit of measure fail")
        pass

    try:
        green_house_gas = row[8].split(" ")
        green_house_gas = green_house_gas[1]
    except IndexError:
        print("green house gas fail")
        pass

    try:
        green_house_gas_uom = row[8].split(" ")
        green_house_gas_uom = green_house_gas_uom[0]
    except IndexError:
        print("green house gas uom fail")
        pass

    try:
        if row[10] == "":
            print("No conversion factor for:")
        else:
            conversion_factor = float(row[10].replace(",", "."))
    except ValueError:
        print("conversion fail")
        pass

    try:
        c_f = FuelsConversionFactors(
            tags=tags,
            unit_of_measure=unit_of_measure,
            green_house_gas=green_house_gas,
            green_house_gas_uom=green_house_gas_uom,
            conversion_factor=conversion_factor,
        )
        c_f.save()
    except UnboundLocalError:
        print("save fail \n")
        pass


def extract_personal_car_distance_conversion_factors(row):
    print_row_description(row)

    try:
        propulsion_type = row[5].lower().replace(" ", "_").replace("-", "_")
    except IndexError:
        print("propulsion type fail")

    try:
        if row[2] == "Cars (by market segment)":
            market_segment = (
                row[3]
                    .lower()
                    .replace("-", "_")
                    .replace("(", "")
                    .replace(")", "")
                    .replace(" ", "_")
                    .replace("___", "_")
                    .replace("__", "_")
            )
            tags = {
                "propulsion_type": propulsion_type,
                "market_segment": market_segment,
            }
        elif row[2] == "Cars (by size)":
            size = (
                row[3]
                    .lower()
                    .replace("-", "_")
                    .replace("(", "")
                    .replace(")", "")
                    .replace(" ", "_")
                    .replace("___", "_")
                    .replace("__", "_")
                    .replace("_car", "")
            )
            tags = {
                "propulsion_type": propulsion_type,
                "size": size,
            }

    except IndexError:
        print("tags fail")
        pass

    try:
        unit_of_measure = row[6]
    except IndexError:
        print("unit of measure fail")
        pass

    try:
        green_house_gas = row[8].split(" ")
        green_house_gas = green_house_gas[1]
    except IndexError:
        print("green house gas fail")
        pass

    try:
        green_house_gas_uom = row[8].split(" ")
        green_house_gas_uom = green_house_gas_uom[0]
    except IndexError:
        print("green house gas uom fail")
        pass

    try:
        conversion_factor = float(row[10].replace(",", "."))
    except ValueError:
        print("conversion fail")
        pass

    try:
        c_f = PersonalCarDistanceConversionFactors(
            tags=tags,
            unit_of_measure=unit_of_measure,
            green_house_gas=green_house_gas,
            green_house_gas_uom=green_house_gas_uom,
            conversion_factor=conversion_factor,
        )
        c_f.save()
    except UnboundLocalError:
        print("save fail")
        pass


def extract_personal_motorbike_distance_conversion_factors(row):
    print_row_description(row)

    try:
        size = (
            row[3]
                .lower()
                .replace("-", "_")
                .replace("(", "")
                .replace(")", "")
                .replace(" ", "_")
                .replace("___", "_")
                .replace("__", "_")
        )

        tags = {"size": size}

    except IndexError:
        print("tags fail")
        pass

    try:
        unit_of_measure = row[6]
    except IndexError:
        print("unit of measure fail")
        pass

    try:
        green_house_gas = row[8].split(" ")
        green_house_gas = green_house_gas[1]
    except IndexError:
        print("green house gas fail")
        pass

    try:
        green_house_gas_uom = row[8].split(" ")
        green_house_gas_uom = green_house_gas_uom[0]
    except IndexError:
        print("green house gas uom fail")
        pass

    try:
        conversion_factor = float(row[10].replace(",", "."))
    except ValueError:
        print("conversion fail")
        pass

    try:
        c_f = PersonalMotorbikeDistanceConversionFactors(
            tags=tags,
            unit_of_measure=unit_of_measure,
            green_house_gas=green_house_gas,
            green_house_gas_uom=green_house_gas_uom,
            conversion_factor=conversion_factor,
        )
        c_f.save()
    except UnboundLocalError:
        print("save fail")
        pass


def extract_delivery_vehicles_heavy_good_vehicle_conversion_factors(row):
    print_row_description(row)
    try:
        laden = "average"
        laden_split = row[5].split(" ")
        if laden_split[0] == "Average":
            pass
        elif laden_split[0] != "Average":
            laden = int(laden_split[0].replace("%", ""))

        # Verify if the truck is refrigated
        refrigerated = False
        refrigerated_split = row[2].split(" ")
        if refrigerated_split[1] == "refrigerated":
            refrigerated = True

        # Trailler info
        trailer_info_raw = row[3]

        # Test if the trailer is rigid or articulated
        trailer_type = "rigid"
        if re.match(r"(?i)rigid", trailer_info_raw):
            pass
        elif not re.match(r"(?i)rigid", trailer_info_raw):
            trailer_type = "articulated"

        # Parse the trailer capacity
        trailer_capacity = "average"
        if re.match(r"(?i)all", trailer_info_raw):
            pass
        elif not re.match(r"(?i)all", trailer_info_raw):
            search = re.search(r"(\()(.*)(\))", trailer_info_raw)

            # Rigid trailer capacity
            if search.group(2) == ">3.5 - 7.5 tonnes":
                trailer_capacity = ">3.5_to_7.5_t"
            elif search.group(2) == ">7.5 tonnes-17 tonnes":
                trailer_capacity = ">7.5_to_17_t"
            elif search.group(2) == ">17 tonnes":
                trailer_capacity = ">17_t"

            # Articulated trailer capacity
            elif search.group(2) == ">3.5 - 33t":
                trailer_capacity = ">3.5_to_33_t"
            elif search.group(2) == ">33t":
                trailer_capacity = ">33_t"
        tags = {
            "laden": laden,
            "refrigerated": refrigerated,
            "trailer_type": trailer_type,
            "trailer_capacity": trailer_capacity,
        }
    except IndexError:
        print("tags fail")
        pass

    try:
        unit_of_measure = row[6]
    except IndexError:
        print("unit of measure fail")
        pass

    try:
        green_house_gas = row[8].split(" ")
        green_house_gas = green_house_gas[1]
    except IndexError:
        print("green house gas fail")
        pass

    try:
        green_house_gas_uom = row[8].split(" ")
        green_house_gas_uom = green_house_gas_uom[0]
    except IndexError:
        print("green house gas uom fail")
        pass

    try:
        conversion_factor = float(row[10].replace(",", "."))
    except ValueError:
        print("conversion fail or no conversion factor")
        print("\n")
        pass

    try:
        c_f = FreightVehiclesHeavyGoodVehicleDistanceConversionFactors(
            tags=tags,
            unit_of_measure=unit_of_measure,
            green_house_gas=green_house_gas,
            green_house_gas_uom=green_house_gas_uom,
            conversion_factor=conversion_factor,
        )
        c_f.save()
    except UnboundLocalError:
        print("save fail")
        pass


def extract_delivery_vehicles_vans_conversion_factors(row):
    print_row_description(row)
    try:
        propulsion_type = (
            row[5]
                .lower()
                .replace("-", "_")
                .replace("(", "")
                .replace(")", "")
                .replace(" ", "_")
                .replace("___", "_")
                .replace("__", "_")
        )

        x = row[3].split(" ")
        if x[0] == "Class":
            if x[1] == "I":
                class_level = "1"
            elif x[1] == "II":
                class_level = "2"
            elif x[1] == "III":
                class_level = "3"
        else:
            class_level = "average"

        tags = {"propulsion_type": propulsion_type, "class": class_level}
    except IndexError:
        print("tags fail")
        pass

    try:
        unit_of_measure = row[6]
    except IndexError:
        print("unit of measure fail")
        pass

    try:
        green_house_gas = row[8].split(" ")
        green_house_gas = green_house_gas[1]
    except IndexError:
        print("green house gas fail")
        pass

    try:
        green_house_gas_uom = row[8].split(" ")
        green_house_gas_uom = green_house_gas_uom[0]
    except IndexError:
        print("green house gas uom fail")
        pass

    try:
        conversion_factor = float(row[10].replace(",", "."))
    except ValueError:
        print("conversion fail or no conversion factor")
        pass

    try:
        c_f = FreightVehiclesVansDistanceConversionFactors(
            tags=tags,
            unit_of_measure=unit_of_measure,
            green_house_gas=green_house_gas,
            green_house_gas_uom=green_house_gas_uom,
            conversion_factor=conversion_factor,
        )
        c_f.save()
    except UnboundLocalError:
        print("save fail")
        pass


if __name__ == "__main__":
    populate_db()
