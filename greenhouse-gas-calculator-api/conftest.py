import pytest

from calculator import create_app


@pytest.fixture
def client():
    app = create_app()
    test_client = app.test_client()

    context = app.app_context()
    context.push()

    yield test_client

    context.pop()
