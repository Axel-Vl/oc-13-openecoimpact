import os

MONGODB_HOST = os.environ.get("MONGODB_HOST")

class Config:
    SECRET_KEY = os.environ.get("SECRET_KEY")


class Dev(Config):
    TESTING = False
    DEBUG = True
    MONGODB_SETTINGS = {
        "host": f"mongodb://{MONGODB_HOST}"
    }  # referencing the docker-compose network DNS


class Test(Config):
    TESTING = True
    DEBUG = True
    MONGODB_SETTINGS = {"host": "mongomock://localhost/test"}


class Prod(Config):
    TESTING = False
    DEBUG = False
