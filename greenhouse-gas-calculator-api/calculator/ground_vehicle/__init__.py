from flask import request
from flask_restx import Resource, Namespace
from ..models import *

api = Namespace(
    "ground-vehicle", description="All resources related to ground vehicles"
)

response_model = api.schema_model(
    "ghg_emission",
    {
        "properties": {
            "CO2e": {
                "properties": {
                    "uom": {"type": "string"},
                    "amount": {"type": "string"},
                },
                "type": "object",
            },
            "CO2": {
                "properties": {
                    "uom": {"type": "string"},
                    "amount": {"type": "string"},
                },
                "type": "object",
            },
            "CH4": {
                "properties": {
                    "uom": {"type": "string"},
                    "amount": {"type": "string"},
                },
                "type": "object",
            },
            "N2O": {
                "properties": {
                    "uom": {"type": "string"},
                    "amount": {"type": "string"},
                },
                "type": "object",
            },
        },
        "type": "object",
    },
)


@api.route("/fuel")
class Fuel(Resource):
    @api.doc(description="description")  # TODO write description
    @api.response(200, "Success", response_model)
    @api.response(204, "No content")
    @api.param(
        "type_of_fuel",
        required=True,
        type=str,
        min=0,
        enum=[
            "diesel_average_biofuel_blend",
            "diesel_100%_mineral_blend",
            "petrol_average_biofuel_blend",
            "petrol_100%_mineral_blend",
            "CNG",
            "LPG",
        ],
    )
    @api.param("amount", required=True, type=float, min=0)
    @api.param(
        "uom",
        "Unit of measure",
        required=True,
        type=str,
        default="litres",
        enum=["litres", "tonnes", "kWh"],
    )
    def get(self):
        uom = request.args.get("uom")
        type_of_fuel = request.args.get("type_of_fuel")
        amount = request.args.get("amount")

        if type_of_fuel == "CNG" or type_of_fuel == "LPG":
            conversion_factors = FuelsConversionFactors.objects(
                unit_of_measure=uom,
                tags={"element_state": "gaseous", "name": type_of_fuel.lower()},
            )
        else:
            conversion_factors = FuelsConversionFactors.objects(
                unit_of_measure=uom,
                tags={"element_state": "liquid", "name": type_of_fuel},
            )

        payload = {}
        for document in conversion_factors:
            conversion_factor = document.conversion_factor
            payload[document.green_house_gas] = {
                "uom": document.green_house_gas_uom,
                "amount": float(amount) * conversion_factor,
            }
        return payload


@api.route("/personal/car/distance")
class PersonalCarDistance(Resource):
    @api.doc(description="description")  # TODO write description
    @api.response(200, "Success", response_model)
    @api.response(204, "No content")
    @api.response(400, "Bad request")
    @api.param(
        "market_segment",
        "This parameter work only with cars, if you supply a value, size parameter will be ignored",
        required=False,
        type=str,
        default=None,
        enum=[
            "mini",
            "supermini",
            "lower_medium",
            "upper_medium",
            "executive",
            "luxury",
            "sports",
            "dual_purpose_4x4",
            "mpv",
        ],
    )
    @api.param(
        "size",
        required=False,
        type=str,
        default="average",
        enum=["average", "small", "medium", "large"],
    )
    @api.param(
        "propulsion_type",
        "This parameter work only with cars, if you supply a value, it allows a more precise calculation",
        required=True,
        type=str,
        default="unknown",
        enum=["unknown", "diesel", "petrol", "cng", "lpg", "hybrid", "plug_in_hybrid"],
    )
    @api.param("distance_travelled", required=True, type=float, min=0)
    @api.param(
        "uom",
        "Unit of measure",
        required=True,
        type=str,
        default="km",
        enum=["km", "miles"],
    )
    def get(self):
        uom = request.args.get("uom")
        distance_travelled = request.args.get("distance_travelled")
        propulsion_type = request.args.get("propulsion_type")
        size = request.args.get("size")
        market_segment = request.args.get("market_segment")

        if not all((uom, distance_travelled, propulsion_type)):
            return "", 400

        tags: dict = {}
        if market_segment:
            tags = {
                "propulsion_type": propulsion_type,
                "market_segment": market_segment,
            }
        elif not market_segment:
            tags = {
                "propulsion_type": propulsion_type,
                "size": size,
            }

        conversion_factors = PersonalCarDistanceConversionFactors.objects(
            unit_of_measure=uom, tags=tags,
        )

        payload: dict = {}
        for document in conversion_factors:
            conversion_factor = document.conversion_factor
            payload[document.green_house_gas] = {
                "uom": document.green_house_gas_uom,
                "amount": float(distance_travelled) * conversion_factor,
            }

        # No conversion factors found
        if payload == {}:
            return "", 204

        return payload


@api.route("/personal/motorbike/distance")
class PersonalMotorbikeDistance(Resource):
    @api.doc(description="description")  # TODO write description
    @api.response(200, "Success", response_model)
    @api.response(204, "No content")
    @api.response(400, "Bad request")
    @api.param(
        "size",
        required=True,
        type=str,
        default="average",
        enum=["average", "small", "medium", "large"],
    )
    @api.param("distance_travelled", required=True, type=float, min=0)
    @api.param(
        "uom",
        "Unit of measure",
        required=True,
        type=str,
        default="km",
        enum=["km", "miles"],
    )
    def get(self):
        uom = request.args.get("uom")
        distance_travelled = request.args.get("distance_travelled")
        size = request.args.get("size")

        if not all((uom, distance_travelled, size)):
            return "", 400

        conversion_factors = PersonalMotorbikeDistanceConversionFactors.objects(
            unit_of_measure=uom, tags={"size": size},
        )

        payload = {}
        for document in conversion_factors:
            conversion_factor = document.conversion_factor
            payload[document.green_house_gas] = {
                "uom": document.green_house_gas_uom,
                "amount": float(distance_travelled) * conversion_factor,
            }

        # No conversion factors found
        if payload == {}:
            return "", 204

        return payload


@api.route("/freight/hgv/distance")
class FreightHeavyGoodVehicleDistance(Resource):
    @api.doc(description="description")  # TODO write description
    @api.response(200, "Success", response_model)
    @api.response(204, "No content")
    @api.response(400, "Bad request")
    @api.param(
        "articulated_trailer_capacity",
        "Capacity values for HGV with articulated trailer",
        required=False,
        type=str,
        enum=["average", ">3.5_to_33_t", ">33_t", ],
    )
    @api.param(
        "rigid_trailer_capacity",
        "Capacity values for HGV with rigid trailer",
        required=False,
        type=str,
        enum=["average", ">3.5_to_7.5_t", ">7.5_to_17_t", ">7.5_to_17_t", ">17_t", ],
    )
    @api.param("refrigerated", required=True, type=bool, default=False)
    @api.param(
        "laden",
        "The % of the total capacity of the vehicle",
        required=True,
        default="average",
        enum=[0, 50, 100, "average"],
    )
    @api.param(
        "trailer_type",
        required=True,
        type=str,
        default="articulated",
        enum=["articulated", "rigid"],
    )
    @api.param("distance_travelled", required=True, type=float, min=0)
    @api.param(
        "uom",
        "Unit of measure",
        required=True,
        type=str,
        default="km",
        enum=["km", "miles"],
    )
    def get(self):
        uom = request.args.get("uom")
        distance_travelled = request.args.get("distance_travelled")
        laden = request.args.get("laden")
        refrigerated = request.args.get("refrigerated")
        trailer_type = request.args.get("trailer_type")
        rigid_trailer_capacity = request.args.get("rigid_trailer_capacity")
        articulated_trailer_capacity = request.args.get("articulated_trailer_capacity")
        tags = {}

        if (
                trailer_type == "rigid"
                and rigid_trailer_capacity
                and not articulated_trailer_capacity
        ):
            tags = {
                "laden": laden,
                "refrigerated": refrigerated,
                "trailer_type": trailer_type,
                "trailer_capacity": rigid_trailer_capacity,
            }
        elif (
                trailer_type == "articulated"
                and articulated_trailer_capacity
                and not rigid_trailer_capacity
        ):
            tags = {
                "laden": laden,
                "refrigerated": refrigerated,
                "trailer_type": trailer_type,
                "trailer_capacity": articulated_trailer_capacity,
            }
        else:
            return "error: trailer capacity queries", 400

        conversion_factors = FreightVehiclesHeavyGoodVehicleDistanceConversionFactors.objects(
            unit_of_measure=uom, tags=tags,
        )

        payload = {}
        for document in conversion_factors:
            conversion_factor = document.conversion_factor
            payload[document.green_house_gas] = {
                "uom": document.green_house_gas_uom,
                "amount": float(distance_travelled) * conversion_factor,
            }
        # No conversion factors found
        if payload == {}:
            return {}, 204

        return payload


@api.route("/freight/van/distance")
class FreightVanDistance(Resource):
    @api.doc(description="description")  # TODO write description
    @api.response(200, "Success", response_model)
    @api.response(204, "No content")
    @api.response(400, "Bad request")
    @api.param(
        "propulsion_type",
        "This parameter work only with cars, if you supply a value, it allows a more precise calculation",
        required=True,
        type=str,
        default="unknown",
        enum=["unknown", "diesel", "petrol", "cng", "lpg", "hybrid", "plug_in_hybrid"],
    )
    @api.param(
        "class",
        required=True,
        type=str,
        default="average",
        enum=["1", "2", "3", "average"],
    )
    @api.param("distance_travelled", required=True, type=float, min=0)
    @api.param(
        "uom",
        "Unit of measure",
        required=True,
        type=str,
        default="km",
        enum=["km", "miles"],
    )
    def get(self):
        uom = request.args.get("uom")
        distance_travelled = request.args.get("distance_travelled")
        van_class = request.args.get("class")
        propulsion_type = request.args.get("propulsion_type")
        if van_class is None or propulsion_type is None:
            return "error: trailer capacity queries", 400

        tags = {"propulsion_type": propulsion_type, "class": van_class}

        conversion_factors = FreightVehiclesVansDistanceConversionFactors.objects(
            unit_of_measure=uom, tags=tags,
        )

        payload = {}
        for document in conversion_factors:
            conversion_factor = document.conversion_factor
            payload[document.green_house_gas] = {
                "uom": document.green_house_gas_uom,
                "amount": float(distance_travelled) * conversion_factor,
            }
        # No conversion factors found
        if payload == {}:
            return "", 204

        return payload
