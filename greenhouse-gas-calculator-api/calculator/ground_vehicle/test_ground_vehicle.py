from calculator.models import *


class TestFuel:
    def test_gaseous_element(self, client):
        tags = {"element_state": "gaseous", "name": "cng"}
        unit_of_measure = "litres"
        green_house_gas = "CO2"
        green_house_gas_uom = "kg"
        conversion_factor = 1
        c_f = FuelsConversionFactors(
            tags=tags,
            unit_of_measure=unit_of_measure,
            green_house_gas=green_house_gas,
            green_house_gas_uom=green_house_gas_uom,
            conversion_factor=conversion_factor,
        )
        c_f.save()

        url = "ground-vehicle/fuel"
        uom = "litres"
        amount = 100
        type_of_fuel = "CNG"
        rv = client.get(f"{url}?uom={uom}&amount={amount}&type_of_fuel={type_of_fuel}")
        assert rv.json == {"CO2": {"amount": 100.0, "uom": "kg"}}

    def test_liquid_element(self, client):
        tags = {"element_state": "liquid", "name": "diesel_average_biofuel_blend"}
        unit_of_measure = "litres"
        green_house_gas = "CO2"
        green_house_gas_uom = "kg"
        conversion_factor = 1
        c_f = FuelsConversionFactors(
            tags=tags,
            unit_of_measure=unit_of_measure,
            green_house_gas=green_house_gas,
            green_house_gas_uom=green_house_gas_uom,
            conversion_factor=conversion_factor,
        )
        c_f.save()

        url = "ground-vehicle/fuel"
        uom = "litres"
        amount = 100
        type_of_fuel = "diesel_average_biofuel_blend"
        rv = client.get(f"{url}?uom={uom}&amount={amount}&type_of_fuel={type_of_fuel}")
        assert rv.json == {"CO2": {"amount": 100.0, "uom": "kg"}}


class TestGroundVehiclePersonalCarDistance:
    """Tests for the personal vehicle distance calculation route"""

    def test_default_value(self, client):
        tags = {
            "propulsion_type": "unknown",
            "size": "average",
        }
        unit_of_measure = "km"
        green_house_gas = "CO2"
        green_house_gas_uom = "kg"
        conversion_factor = 1
        c_f = PersonalCarDistanceConversionFactors(
            tags=tags,
            unit_of_measure=unit_of_measure,
            green_house_gas=green_house_gas,
            green_house_gas_uom=green_house_gas_uom,
            conversion_factor=conversion_factor,
        )
        c_f.save()

        url = "ground-vehicle/personal/car/distance"
        uom = "km"
        distance_travelled = 100
        size = "average"
        propulsion_type = "unknown"
        rv = client.get(
            f"{url}?uom={uom}&distance_travelled={distance_travelled}&size={size}&propulsion_type={propulsion_type}"
        )
        assert rv.json == {"CO2": {"amount": 100.0, "uom": "kg"}}

    def test_market_segment_mini(self, client):
        tags = {
            "propulsion_type": "unknown",
            "market_segment": "mini",
        }
        unit_of_measure = "km"
        green_house_gas = "CO2"
        green_house_gas_uom = "kg"
        conversion_factor = 1
        c_f = PersonalCarDistanceConversionFactors(
            tags=tags,
            unit_of_measure=unit_of_measure,
            green_house_gas=green_house_gas,
            green_house_gas_uom=green_house_gas_uom,
            conversion_factor=conversion_factor,
        )
        c_f.save()

        url = "ground-vehicle/personal/car/distance"
        uom = "km"
        distance_travelled = 100
        market_segment = "mini"
        propulsion_type = "unknown"
        rv = client.get(
            f"{url}?uom={uom}&distance_travelled={distance_travelled}&market_segment={market_segment}&propulsion_type={propulsion_type}"
        )
        assert rv.json == {"CO2": {"amount": 100.0, "uom": "kg"}}

    def test_no_data_response(self, client):
        url = "ground-vehicle/personal/car/distance"
        uom = "km"
        distance_travelled = 100
        size = "enormous"
        propulsion_type = "unknown"
        rv = client.get(
            f"{url}?uom={uom}&distance_travelled={distance_travelled}&size={size}&propulsion_type={propulsion_type}"
        )
        assert rv.status_code == 204

    def test_bad_request(self, client):
        url = "ground-vehicle/personal/car/distance"
        uom = "km"
        size = "average"
        propulsion_type = "unknown"
        distance_travelled = 100
        rv = client.get(f"{url}?uom={uom}&distance_travelled={distance_travelled}")
        assert rv.status_code == 400


class TestGroundVehiclePersonalMotorbikeDistance:
    def test_default_value(self, client):
        tags = {
            "size": "average",
        }
        unit_of_measure = "km"
        green_house_gas = "CO2"
        green_house_gas_uom = "kg"
        conversion_factor = 1
        c_f = PersonalMotorbikeDistanceConversionFactors(
            tags=tags,
            unit_of_measure=unit_of_measure,
            green_house_gas=green_house_gas,
            green_house_gas_uom=green_house_gas_uom,
            conversion_factor=conversion_factor,
        )
        c_f.save()

        url = "ground-vehicle/personal/motorbike/distance"
        uom = "km"
        distance_travelled = 100
        size = "average"
        rv = client.get(
            f"{url}?uom={uom}&distance_travelled={distance_travelled}&size={size}"
        )
        assert rv.json == {"CO2": {"amount": 100.0, "uom": "kg"}}

    def test_no_data_response(self, client):
        url = "ground-vehicle/personal/motorbike/distance"
        uom = "km"
        distance_travelled = 100
        size = "enormous"
        rv = client.get(
            f"{url}?uom={uom}&distance_travelled={distance_travelled}&size={size}"
        )
        assert rv.status_code == 204

    def test_bad_request(self, client):
        url = "ground-vehicle/personal/motorbike/distance"
        uom = "km"
        distance_travelled = 100
        rv = client.get(f"{url}?uom={uom}&distance_travelled={distance_travelled}")
        assert rv.status_code == 400


class TestGroundVehicleFreightHeavyGoodVehiclesDistance:
    def test_default_value_rigid_trailer(self, client):
        tags = {
            "laden": "0",
            "refrigerated": "false",
            "trailer_type": "rigid",
            "trailer_capacity": "average",
        }
        unit_of_measure = "km"
        green_house_gas = "CO2"
        green_house_gas_uom = "kg"
        conversion_factor = 1
        c_f = FreightVehiclesHeavyGoodVehicleDistanceConversionFactors(
            tags=tags,
            unit_of_measure=unit_of_measure,
            green_house_gas=green_house_gas,
            green_house_gas_uom=green_house_gas_uom,
            conversion_factor=conversion_factor,
        )
        c_f.save()

        url = "ground-vehicle/freight/hgv/distance"
        uom = "km"
        distance_travelled = 100
        size = "average"
        rv = client.get(
            f"{url}?uom={uom}&distance_travelled=100&trailer_type=rigid&laden=0&refrigerated=false&rigid_trailer_capacity=average"
        )
        assert rv.json == {"CO2": {"amount": 100.0, "uom": "kg"}}

    def test_default_value_articulated_trailer(self, client):
        tags = {
            "laden": "0",
            "refrigerated": "false",
            "trailer_type": "articulated",
            "trailer_capacity": "average",
        }
        unit_of_measure = "km"
        green_house_gas = "CO2"
        green_house_gas_uom = "kg"
        conversion_factor = 1
        c_f = FreightVehiclesHeavyGoodVehicleDistanceConversionFactors(
            tags=tags,
            unit_of_measure=unit_of_measure,
            green_house_gas=green_house_gas,
            green_house_gas_uom=green_house_gas_uom,
            conversion_factor=conversion_factor,
        )
        c_f.save()

        url = "ground-vehicle/freight/hgv/distance"
        uom = "km"
        distance_travelled = 100
        size = "average"
        rv = client.get(
            f"{url}?uom={uom}&distance_travelled=100&trailer_type=articulated&laden=0&refrigerated=false&articulated_trailer_capacity=average"
        )
        assert rv.json == {"CO2": {"amount": 100.0, "uom": "kg"}}

    def test_no_data_response(self, client):
        url = "ground-vehicle/freight/hgv/distance"
        uom = "km"
        distance_travelled = 100
        size = "average"
        rv = client.get(
            f"{url}?uom={uom}&distance_travelled=100&trailer_type=articulated&laden=0&refrigerated=false&articulated_trailer_capacity=0"
        )
        assert rv.status_code == 204

    def test_bad_request(self, client):
        url = "ground-vehicle/freight/hgv/distance"
        uom = "km"
        distance_travelled = 100
        size = "average"
        rv = client.get(
            f"{url}?uom={uom}&distance_travelled=100&trailer_type=articulated&laden=0&refrigerated=false&articulated_trailer_capacity=0&rigid_trailer_capacity=0"
        )
        assert rv.status_code == 400


class TestGroundVehicleFreightVanDistance:
    def test_default_value(self, client):
        tags = {
            "class": "average",
            "propulsion_type": "unknown",
        }
        unit_of_measure = "km"
        green_house_gas = "CO2"
        green_house_gas_uom = "kg"
        conversion_factor = 1
        c_f = FreightVehiclesVansDistanceConversionFactors(
            tags=tags,
            unit_of_measure=unit_of_measure,
            green_house_gas=green_house_gas,
            green_house_gas_uom=green_house_gas_uom,
            conversion_factor=conversion_factor,
        )
        c_f.save()

        url = "ground-vehicle/freight/van/distance"
        uom = "km"
        distance_travelled = 100
        van_class = "average"
        propulsion_type = "unknown"
        rv = client.get(
            f"{url}?uom={uom}&distance_travelled={distance_travelled}&class={van_class}&propulsion_type={propulsion_type}"
        )
        assert rv.json == {"CO2": {"amount": 100.0, "uom": "kg"}}

    def test_no_data_response(self, client):
        url = "ground-vehicle/freight/van/distance"
        uom = "km"
        distance_travelled = 100
        van_class = "average"
        propulsion_type = "diesel"
        rv = client.get(
            f"{url}?uom={uom}&distance_travelled={distance_travelled}&class={van_class}&propulsion_type={propulsion_type}"
        )
        assert rv.status_code == 204

    def test_bad_request(self, client):
        url = "ground-vehicle/freight/van/distance"
        uom = "km"
        distance_travelled = 100
        rv = client.get(f"{url}?uom={uom}&distance_travelled={distance_travelled}")
        assert rv.status_code == 400
