import os
import sentry_sdk
from flask import Flask
from flask_cors import CORS
from sentry_sdk.integrations.flask import FlaskIntegration

SENTRY_DSN = os.environ.get("SENTRY_DSN")
ENV = os.environ.get("ENV")
sentry_sdk.init(
    dsn=SENTRY_DSN,
    integrations=[FlaskIntegration()],
    traces_sample_rate=1.0,
    environment=ENV
)


def create_app():
    app = Flask(__name__, instance_relative_config=False)
    CORS(app)

    if ENV == "dev":
        app.config.from_object("config.Dev")
    elif ENV == "test":
        app.config.from_object("config.Test")
    elif ENV == "prod":
        app.config.from_object("config.Prod")

    from calculator.models import db
    from .api import api

    db.init_app(app)
    api.init_app(app)

    return app
