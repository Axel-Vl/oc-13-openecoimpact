from flask_mongoengine import MongoEngine

db = MongoEngine()


class FuelsConversionFactors(db.Document):
    tags = db.DictField()
    unit_of_measure = db.StringField(required=True)
    green_house_gas = db.StringField(required=True)
    green_house_gas_uom = db.StringField(required=True)
    conversion_factor = db.FloatField(required=True)


class PersonalCarDistanceConversionFactors(db.Document):
    tags = db.DictField()
    unit_of_measure = db.StringField(required=True)
    green_house_gas = db.StringField(required=True)
    green_house_gas_uom = db.StringField(required=True)
    conversion_factor = db.FloatField(required=True)


class PersonalMotorbikeDistanceConversionFactors(db.Document):
    tags = db.DictField()
    unit_of_measure = db.StringField(required=True)
    green_house_gas = db.StringField(required=True)
    green_house_gas_uom = db.StringField(required=True)
    conversion_factor = db.FloatField(required=True)


class FreightVehiclesVansDistanceConversionFactors(db.Document):
    tags = db.DictField()
    unit_of_measure = db.StringField(required=True)
    green_house_gas = db.StringField(required=True)
    green_house_gas_uom = db.StringField(required=True)
    conversion_factor = db.FloatField(required=True)


class FreightVehiclesHeavyGoodVehicleDistanceConversionFactors(db.Document):
    tags = db.DictField()
    unit_of_measure = db.StringField(required=True)
    green_house_gas = db.StringField(required=True)
    green_house_gas_uom = db.StringField(required=True)
    conversion_factor = db.FloatField(required=True)
