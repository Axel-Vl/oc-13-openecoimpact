from flask_restx import Api

from .ground_vehicle import api as ground_vehicles

api = Api(
    title='Open eco impact'
)
api.add_namespace(ground_vehicles)
